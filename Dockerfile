# Image base
FROM node:latest

# Directorio de la app en el contenedor
WORKDIR /app

# Copiado de archivos
ADD build/default /app/build/default
ADD server.js /app
ADD package.json /app

# Dependencias
# para descargar dependencias especificadas en el package.json
RUN npm install

# Puerto que sera expuesto en el contenedor
EXPOSE 3000

# Comando para ejecutar el servicio
CMD ["npm","start"]
